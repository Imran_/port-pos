
package com.gp.pos.thirdparties;

import javax.swing.ListCellRenderer;

import com.gp.data.gui.ListCellRendererBasic;
import com.gp.data.loader.ComparatorCreator;
import com.gp.data.loader.TableDefinition;
import com.gp.data.loader.Vectorer;
import com.gp.data.user.EditorRecord;
import com.gp.data.user.ListProvider;
import com.gp.data.user.ListProviderCreator;
import com.gp.data.user.SaveProvider;
import com.gp.pos.forms.AppLocal;
import com.gp.pos.panels.*;

public class ThirdPartiesPanel extends JPanelTable {
    
    private TableDefinition tthirdparties;
    private ThirdPartiesView jeditor;
    
    /** Creates a new instance of JPanelPeople */
    public ThirdPartiesPanel() {
    }
    
    protected void init() {
        DataLogicThirdParties dlThirdParties = (DataLogicThirdParties) app.getBean("com.openbravo.pos.thirdparties.DataLogicThirdParties");        
        tthirdparties = dlThirdParties.getTableThirdParties();        
        jeditor = new ThirdPartiesView(app, dirty);     
    }
    
    public ListProvider getListProvider() {
        return new ListProviderCreator(tthirdparties);
    }
    
    public SaveProvider getSaveProvider() {
        return new SaveProvider(tthirdparties);      
    }
    
    public Vectorer getVectorer() {
        return tthirdparties.getVectorerBasic(new int[]{1, 2, 3, 4});
    }
    
    public ComparatorCreator getComparatorCreator() {
        return tthirdparties.getComparatorCreator(new int[] {1, 2, 3, 4});
    }
    
    public ListCellRenderer getListCellRenderer() {
        return new ListCellRendererBasic(tthirdparties.getRenderStringBasic(new int[]{1, 2}));
    }
    
    public EditorRecord getEditor() {
        return jeditor;
    }       
    
    public String getTitle() {
        return AppLocal.getIntString("Menu.ThirdPartiesManagement");
    }     
}
