//    gp POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 gp, S.L.
//    http://www.gp.com/product/pos
//
//    This file is part of gp POS.
//
//    gp POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//
//    You should have received a copy of the GNU General Public License
//    along with gp POS.  If not, see <http://www.gnu.org/licenses/>.

package com.gp.pos.inventory;

import com.gp.data.loader.Datas;
import com.gp.data.model.Column;
import com.gp.data.model.Field;
import com.gp.data.model.PrimaryKey;
import com.gp.data.model.Row;
import com.gp.data.model.Table;
import com.gp.data.user.EditorRecord;
import com.gp.format.Formats;
import com.gp.pos.forms.AppLocal;
import com.gp.pos.panels.JPanelTable2;

/**
 *
 * @author adrianromero
 */
public class AttributeSetsPanel extends JPanelTable2 {

    private EditorRecord editor;

    /** Creates a new instance of JPanelCategories */
    public AttributeSetsPanel() {
    }

    protected void init() {

        row = new Row(
                new Field("ID", Datas.STRING, Formats.STRING),
                new Field(AppLocal.getIntString("Label.Name"), Datas.STRING, Formats.STRING, true, true, true)
        );

        Table table = new Table(
                "ATTRIBUTESET",
                new PrimaryKey("ID"),
                new Column("NAME"));

        lpr = row.getListProvider(app.getSession(), table);
        spr = row.getSaveProvider(app.getSession(), table);

        editor = new AttributeSetsEditor(dirty);
    }

    public EditorRecord getEditor() {
        return editor;
    }

    public String getTitle() {
        return AppLocal.getIntString("Menu.AttributeSets");
    }
}
