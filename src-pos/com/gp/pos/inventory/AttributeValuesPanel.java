//    gp POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 gp, S.L.
//    http://www.gp.com/product/pos
//
//    This file is part of gp POS.
//
//    gp POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    gp POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with gp POS.  If not, see <http://www.gnu.org/licenses/>.

package com.gp.pos.inventory;

import com.gp.pos.panels.JPanelTable2;
import com.gp.basic.BasicException;
import com.gp.data.loader.Datas;
import com.gp.data.model.Column;
import com.gp.data.model.Field;
import com.gp.data.model.PrimaryKey;
import com.gp.data.model.Row;
import com.gp.data.model.Table;
import com.gp.data.user.EditorRecord;
import com.gp.format.Formats;
import com.gp.pos.forms.AppLocal;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author adrianromero
 */
public class AttributeValuesPanel extends JPanelTable2 {

    private AttributeValuesEditor editor;
    private AttributeFilter filter;

    protected void init() {

        filter = new AttributeFilter();
        filter.init(app);
        filter.addActionListener(new ReloadActionListener());

        row = new Row(
                new Field("ID", Datas.STRING, Formats.STRING),
                new Field("ATTRIBUTE_ID", Datas.STRING, Formats.STRING),
                new Field(AppLocal.getIntString("label.value"), Datas.STRING, Formats.STRING, true, true, true)
        );

        Table table = new Table(
                "ATTRIBUTEVALUE",
                new PrimaryKey("ID"),
                new Column("ATTRIBUTE_ID"),
                new Column("VALUE"));

        lpr = row.getListProvider(app.getSession(),
                "SELECT ID, ATTRIBUTE_ID, VALUE FROM ATTRIBUTEVALUE WHERE ATTRIBUTE_ID = ? ", filter);
        spr = row.getSaveProvider(app.getSession(), table);

        editor = new AttributeValuesEditor(dirty);
    }

    @Override
    public void activate() throws BasicException {
        filter.activate();

        //super.activate();
        startNavigation();
        reload();
    }

    @Override
    public Component getFilter(){
        return filter.getComponent();
    }

    public EditorRecord getEditor() {
        return editor;
    }

    private void reload() throws BasicException {

        String attid = (String) filter.createValue();
        editor.setInsertId(attid); // must be set before load
        bd.setEditable(attid != null);
        bd.actionLoad();
    }

    public String getTitle() {
        return AppLocal.getIntString("Menu.AttributeValues");
    }

    private class ReloadActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                reload();
            } catch (BasicException w) {
            }
        }
    }
}