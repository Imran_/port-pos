//    gp POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 gp, S.L.
//    http://www.gp.com/product/pos
//
//    This file is part of gp POS.
//
//    gp POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    gp POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with gp POS.  If not, see <http://www.gnu.org/licenses/>.

package com.gp.possync;

import com.gp.pos.forms.AppView;
import com.gp.pos.forms.BeanFactoryCache;
import com.gp.pos.forms.BeanFactoryException;
import com.gp.pos.forms.DataLogicSystem;

/**
 *
 * @author adrian
 */
public class OrdersSyncCreate extends BeanFactoryCache {
    
    public Object constructBean(AppView app) throws BeanFactoryException {

        DataLogicSystem dlSystem = (DataLogicSystem) app.getBean("com.gp.pos.forms.DataLogicSystem");
        DataLogicIntegration dli = (DataLogicIntegration) app.getBean("com.gp.possync.DataLogicIntegration");

        OrdersSync bean = new OrdersSync(dlSystem, dli);
        return bean;
    }
}
